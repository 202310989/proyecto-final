<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PanelController;
use App\Http\Controllers\Burbujacontroller;
use App\Http\Controllers\QuickshortController;
use App\Http\Controllers\ProyectoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/inicio-panel', [PanelController::class, 'InicioPanel']);
Route::get('/proyecto-login', [ProyectoController::class, 'ProyectoLogin']);
Route::get('/ordenamiento-burbuja', [Burbujacontroller::class, 'OrdenamietoBurbuja']);
Route::get('/ordenamiento-quick-short', [QuickshortController::class, 'OrdenamietoQuickShort']);

